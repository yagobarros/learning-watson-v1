const port = 3000;

const express = require('express');
const bodyParser = require('body-parser');
const assistantV1 = require('watson-developer-cloud/assistant/v1');
const watsonAuth = require('./watson-auth');
const cors = require('cors');

const app = express();
const assistant = new assistantV1({
    username: watsonAuth.username,
    password: watsonAuth.password,
    url: 'https://gateway.watsonplatform.net/assistant/api',
    version: '2018-02-16'
})
app.use(bodyParser.json());
app.use(cors());

app.post('/dialog', (req, res) => {
    const { message } = req.body;
    assistant.message({
        input: { text: message},
        workspace_id: watsonAuth.workspace_id
    }, (err, response) => {
        if(err){ 
            console.log(err);
        } else  { 
            const { output } = response;
            return res.json(output.text);
        }
    })
})

app.listen(port, console.log(`Running port: ${port}`));